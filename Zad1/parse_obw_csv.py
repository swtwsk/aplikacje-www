import os
import csv
import django
from django.db import transaction

os.environ.setdefault("DJANGO_SETTINGS_MODULE", "Zad1.settings")
django.setup()

from wybory.models import *

csv_dir_path = "csv/obw"


def przygotuj_nazwisko(nazwisko):
    czesci = nazwisko.split('-')
    nowe_czesci = []
    for czesc in czesci:
        if len(czesc) > 1:
            nowe_czesci.append(czesc[0].upper() + czesc[1:].lower())
        else:
            nowe_czesci.append(czesc[0].upper())
    return '-'.join(nowe_czesci)


def parsuj_csv(sciezka):
    plik_csv = open(sciezka, 'r', encoding='utf-8')
    reader = csv.reader(plik_csv, dialect='excel', delimiter=',')
    # parse header row
    kandydat_kolumna = {}
    pierwszy_wiersz = next(reader)
    for i in range(12, len(pierwszy_wiersz)):
        kandydat_kolumna[i] = przygotuj_nazwisko(pierwszy_wiersz[i].split()[-1])

    with transaction.atomic():
        for wiersz in reader:
            nr_okr = wiersz[0]
            kod_gminy = wiersz[1]
            nazwa_gminy = wiersz[2]
            powiat = wiersz[3]
            nr_obw = wiersz[4]
            uprawnieni = wiersz[7]
            wydane_karty = wiersz[8]
            glosy_oddane = wiersz[9]
            glosy_wazne = wiersz[11]

            p, _ = Powiat.objects.get_or_create(nazwa=powiat, okreg=Okreg.objects.get(nr_okregu=nr_okr))
            g, _ = Gmina.objects.get_or_create(kod_gminy=kod_gminy, nazwa=nazwa_gminy, powiat=p)
            o, _ = Obwod.objects.get_or_create(nr_obw=nr_obw, gmina=g, uprawnieni=uprawnieni, wydane_karty=wydane_karty,
                                               glosy_oddane=glosy_oddane, glosy_wazne=glosy_wazne)

            for nr_kolumny, nazwisko in kandydat_kolumna.items():
                k = Kandydat.objects.get(nazwisko__iexact=nazwisko)
                WynikKandydata.objects.create(kandydat=k, obwod=o, glosy=wiersz[nr_kolumny])


# Iterate over csv
for plik in os.listdir(csv_dir_path):
    nazwa_pliku = os.fsdecode(plik)
    if nazwa_pliku.endswith(".csv"):
        print(nazwa_pliku)
        sciezka_do_pliku = os.path.join(csv_dir_path, nazwa_pliku)
        parsuj_csv(sciezka_do_pliku)
