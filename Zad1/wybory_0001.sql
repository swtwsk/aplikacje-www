BEGIN;
--
-- Create model Gmina
--
CREATE TABLE "wybory_gmina" (
    "id" integer NOT NULL PRIMARY KEY AUTOINCREMENT,
    "kod_gminy" integer NOT NULL,
    "nazwa" varchar(50) NOT NULL
);
--
-- Create model Kandydat
--
CREATE TABLE "wybory_kandydat" (
    "id" integer NOT NULL PRIMARY KEY AUTOINCREMENT,
    "imie" varchar(21) NOT NULL,
    "nazwisko" varchar(21) NOT NULL
);
--
-- Create model Obwod
--
CREATE TABLE "wybory_obwod" (
    "id" integer NOT NULL PRIMARY KEY AUTOINCREMENT,
    "nr_obw" integer NOT NULL,
    "uprawnieni" integer NOT NULL,
    "wydane_karty" integer NOT NULL,
    "glosy_oddane" integer NOT NULL,
    "glosy_wazne" integer NOT NULL,
    "gmina_id" integer NOT NULL 
        REFERENCES "wybory_gmina" ("id")
        DEFERRABLE INITIALLY DEFERRED
);
--
-- Create model Okreg
--
CREATE TABLE "wybory_okreg" (
    "id" integer NOT NULL PRIMARY KEY AUTOINCREMENT,
    "nazwa" varchar(50) NOT NULL,
    "nr_okregu" integer NOT NULL
);
--
-- Create model Powiat
--
CREATE TABLE "wybory_powiat" (
    "id" integer NOT NULL PRIMARY KEY AUTOINCREMENT,
    "nazwa" varchar(50) NOT NULL,
    "okreg_id" integer NOT NULL 
        REFERENCES "wybory_okreg" ("id")
        DEFERRABLE INITIALLY DEFERRED
);
--
-- Create model Wojewodztwo
--
CREATE TABLE "wybory_wojewodztwo" (
    "id" integer NOT NULL PRIMARY KEY AUTOINCREMENT,
    "nazwa" varchar(21) NOT NULL
);
--
-- Create model WynikKandydata
--
CREATE TABLE "wybory_wynikkandydata" (
    "id" integer NOT NULL PRIMARY KEY AUTOINCREMENT,
    "glosy" integer NOT NULL,
    "kandydat_id" integer NOT NULL
        REFERENCES "wybory_kandydat" ("id") 
        DEFERRABLE INITIALLY DEFERRED,
    "obwod_id" integer NOT NULL 
        REFERENCES "wybory_obwod" ("id")
        DEFERRABLE INITIALLY DEFERRED
);
--
-- Add field wojewodztwo to okreg
--
ALTER TABLE "wybory_okreg" RENAME TO "wybory_okreg__old";

CREATE TABLE "wybory_okreg" (
    "id" integer NOT NULL PRIMARY KEY AUTOINCREMENT,
    "nazwa" varchar(50) NOT NULL,
    "nr_okregu" integer NOT NULL,
    "wojewodztwo_id" integer NOT NULL
        REFERENCES "wybory_wojewodztwo" ("id")
        DEFERRABLE INITIALLY DEFERRED
);
INSERT INTO "wybory_okreg" ("wojewodztwo_id", "id", "nazwa", "nr_okregu")
    SELECT NULL, "id", "nazwa", "nr_okregu" 
    FROM "wybory_okreg__old";
DROP TABLE "wybory_okreg__old";

CREATE INDEX "wybory_obwod_gmina_id_5e2078a8" 
    ON "wybory_obwod" ("gmina_id");
CREATE INDEX "wybory_powiat_okreg_id_87ff9688" 
    ON "wybory_powiat" ("okreg_id");
CREATE INDEX "wybory_wynikkandydata_kandydat_id_7e07e5a5" 
    ON "wybory_wynikkandydata" ("kandydat_id");
CREATE INDEX "wybory_wynikkandydata_obwod_id_1923b25d" 
    ON "wybory_wynikkandydata" ("obwod_id");
CREATE INDEX "wybory_okreg_wojewodztwo_id_1c1caa45" 
    ON "wybory_okreg" ("wojewodztwo_id");
--
-- Add field powiat to gmina
--
ALTER TABLE "wybory_gmina" RENAME TO "wybory_gmina__old";
CREATE TABLE "wybory_gmina" (
    "id" integer NOT NULL PRIMARY KEY AUTOINCREMENT,
    "kod_gminy" integer NOT NULL,
    "nazwa" varchar(50) NOT NULL,
    "powiat_id" integer NOT NULL 
        REFERENCES "wybory_powiat" ("id") 
        DEFERRABLE INITIALLY DEFERRED
);
INSERT INTO "wybory_gmina" ("powiat_id", "id", "kod_gminy", "nazwa") 
    SELECT NULL, "id", "kod_gminy", "nazwa" 
    FROM "wybory_gmina__old";
DROP TABLE "wybory_gmina__old";

CREATE INDEX "wybory_gmina_powiat_id_8d23ac3a" 
    ON "wybory_gmina" ("powiat_id");
    
COMMIT;
