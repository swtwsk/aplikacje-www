from django import template

register = template.Library()


@register.filter
def float_divide(value, args):
    return "{0:.2f}".format(round(value / args * 100.0, 2)).replace('.', ',')


@register.filter
def sub(value, args):
    return value - args
