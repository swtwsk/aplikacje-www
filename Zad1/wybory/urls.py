from django.urls import path
from . import views

app_name = 'wybory'

urlpatterns = [
    path('', views.kraj_html, name='index'),
    path('<str:wojewodztwo_nazwa>/', views.wojewodztwo_html, name='wojewodztwo'),
    path('<str:wojewodztwo_nazwa>/<int:nr_okregu>/', views.okreg_html, name='okreg'),
    path('<str:wojewodztwo_nazwa>/<int:nr_okregu>/<str:powiat_nazwa>/', views.powiat_html, name='powiat'),
    path('<str:wojewodztwo_nazwa>/<int:nr_okregu>/<str:powiat_nazwa>/<str:kod_gminy>/', views.gmina_html, name='gmina'),
    path('<str:wojewodztwo_nazwa>/<int:nr_okregu>/<str:powiat_nazwa>/<str:kod_gminy>/<int:nr_obwodu>/',
         views.obwod_html, name='obwod'),
]
