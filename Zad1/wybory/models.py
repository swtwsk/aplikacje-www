from django.db import models


class Kandydat(models.Model):
    imie = models.CharField(max_length=21)
    nazwisko = models.CharField(max_length=21)

    class Meta:
        unique_together = ("imie", "nazwisko")

    def __str__(self):
        return self.nazwisko.upper() + " " + self.imie


# TODO: choose ON_DELETE action
class Wojewodztwo(models.Model):
    nazwa = models.CharField(max_length=21, unique=True)

    def __str__(self):
        return "Woj. " + self.nazwa


class Okreg(models.Model):
    nazwa = models.CharField(max_length=50)
    nr_okregu = models.IntegerField(unique=True)
    wojewodztwo = models.ForeignKey(Wojewodztwo, on_delete=models.CASCADE)

    def __str__(self):
        return self.nazwa + " (okręg nr " + str(self.nr_okregu) + ")"


class Powiat(models.Model):
    nazwa = models.CharField(max_length=50)
    okreg = models.ForeignKey(Okreg, on_delete=models.CASCADE)

    def __str__(self):
        return "Powiat " + self.nazwa


class Gmina(models.Model):
    kod_gminy = models.CharField(max_length=6)
    nazwa = models.CharField(max_length=50)  # TODO: check max length of 'gmina' field
    powiat = models.ForeignKey(Powiat, on_delete=models.CASCADE)

    def __str__(self):
        return self.nazwa


class Obwod(models.Model):
    nr_obw = models.IntegerField()
    gmina = models.ForeignKey(Gmina, on_delete=models.CASCADE)
    uprawnieni = models.IntegerField()
    wydane_karty = models.IntegerField()
    glosy_oddane = models.IntegerField()
    glosy_wazne = models.IntegerField()

    class Meta:
        unique_together = ("nr_obw", "gmina")

    def __str__(self):
        return str(self.gmina) + ", obwód nr " + str(self.nr_obw)


class WynikKandydata(models.Model):
    kandydat = models.ForeignKey(Kandydat, db_index=True, on_delete=models.CASCADE)
    obwod = models.ForeignKey(Obwod, on_delete=models.CASCADE)
    glosy = models.IntegerField()

    class Meta:
        unique_together = ("kandydat", "obwod")

    def __str__(self):
        return str(self.kandydat) + ", " + str(self.obwod) + " - " + str(self.glosy) + " głosów"
