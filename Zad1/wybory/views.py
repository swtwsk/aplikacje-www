from django.shortcuts import render
from django.http import Http404

from .logika import *
from .models import *


def kraj_html(request):
    obwody = Obwod.objects.all()

    jednostki = [
    ]
    linki = [
        ('/' + wojewodztwo.nazwa.lower() + '/', str(wojewodztwo)) for wojewodztwo in Wojewodztwo.objects.all()
    ]
    return render(request, 'wybory/wyniki.html', przygotuj_context(
        'Polska', 'kraju', jednostki, linki, 'POLSKA', wyniki_kraju(), obwody))


def wojewodztwo_html(request, wojewodztwo_nazwa):
    try:
        wojewodztwo = Wojewodztwo.objects.get(nazwa=wojewodztwo_nazwa.upper())
    except Wojewodztwo.DoesNotExist:
        raise Http404("Województwo nie istnieje")

    obwody = Obwod.objects.filter(gmina__powiat__okreg__wojewodztwo=wojewodztwo)

    jednostki = [
        ('/', 'POLSKA'),
    ]
    linki = [
        ('/' + wojewodztwo_nazwa + '/' + str(okreg.nr_okregu) + '/', str(okreg))
        for okreg in Okreg.objects.filter(wojewodztwo=wojewodztwo)
    ]
    return render(request, 'wybory/wyniki.html', przygotuj_context(
        str(wojewodztwo), 'województwie', jednostki, linki, wojewodztwo_nazwa.upper(),
        wyniki_wojewodztwa(wojewodztwo), obwody))


def okreg_html(request, wojewodztwo_nazwa, nr_okregu):
    try:
        okreg = Okreg.objects.get(nr_okregu=nr_okregu)
    except Okreg.DoesNotExist:
        raise Http404("Okręg nie istnieje")
    if okreg.wojewodztwo.nazwa.lower() != wojewodztwo_nazwa:
        raise Http404("Okręg nie istnieje")

    obwody = Obwod.objects.filter(gmina__powiat__okreg=okreg)

    jednostki = [
        ('/', 'POLSKA'),
        ('/' + wojewodztwo_nazwa + '/', wojewodztwo_nazwa.upper()),
    ]
    linki = [
        ('/' + wojewodztwo_nazwa + '/' + str(okreg.nr_okregu) + '/' + powiat.nazwa + '/', str(powiat))
        for powiat in Powiat.objects.filter(okreg=okreg)
    ]
    return render(request, 'wybory/wyniki.html', przygotuj_context(
        str(okreg), 'okręgu', jednostki, linki, okreg.nazwa.upper(), wyniki_okregu(okreg), obwody))


def powiat_html(request, wojewodztwo_nazwa, nr_okregu, powiat_nazwa):
    try:
        powiat = Powiat.objects.get(nazwa=powiat_nazwa)
    except Powiat.DoesNotExist:
        raise Http404("Powiat nie istnieje")
    try:
        okreg = Okreg.objects.get(nr_okregu=nr_okregu)
    except Okreg.DoesNotExist:
        raise Http404("Powiat nie istnieje")
    if powiat.okreg != okreg or okreg.wojewodztwo.nazwa.lower() != wojewodztwo_nazwa:
        raise Http404("Powiat nie istnieje")

    obwody = Obwod.objects.filter(gmina__powiat=powiat)

    jednostki = [
        ('/', 'POLSKA'),
        ('/' + wojewodztwo_nazwa + '/', wojewodztwo_nazwa.upper()),
        ('/' + wojewodztwo_nazwa + '/' + str(nr_okregu) + '/', okreg.nazwa.upper())
    ]
    linki = [
        ('/' + wojewodztwo_nazwa + '/' + str(okreg.nr_okregu) + '/' + powiat_nazwa + '/' + str(gmina.kod_gminy) + '/',
         str(gmina))
        for gmina in Gmina.objects.filter(powiat=powiat)
    ]
    return render(request, 'wybory/wyniki.html', przygotuj_context(
        str(powiat), 'powiecie', jednostki, linki, powiat.nazwa.upper(), wyniki_powiatu(powiat), obwody))


def gmina_html(request, wojewodztwo_nazwa, nr_okregu, powiat_nazwa, kod_gminy):
    try:
        gmina = Gmina.objects.get(kod_gminy=kod_gminy)
    except Gmina.DoesNotExist:
        raise Http404("Gmina nie istnieje")
    try:
        okreg = Okreg.objects.get(nr_okregu=nr_okregu)
    except Okreg.DoesNotExist:
        raise Http404("Gmina nie istnieje")
    try:
        powiat = Powiat.objects.get(nazwa=powiat_nazwa)
    except Powiat.DoesNotExist:
        raise Http404("Gmina nie istnieje")
    if gmina.powiat != powiat or powiat.okreg != okreg or okreg.wojewodztwo.nazwa.lower() != wojewodztwo_nazwa:
        raise Http404("Gmina nie istnieje")

    obwody = Obwod.objects.filter(gmina=gmina)

    jednostki = [
        ('/', 'POLSKA'),
        ('/' + wojewodztwo_nazwa + '/', wojewodztwo_nazwa.upper()),
        ('/' + wojewodztwo_nazwa + '/' + str(nr_okregu) + '/', okreg.nazwa.upper()),
        ('/' + wojewodztwo_nazwa + '/' + str(nr_okregu) + '/' + powiat_nazwa + '/', powiat_nazwa.upper())
    ]
    linki = [
        ('/' + wojewodztwo_nazwa + '/' + str(okreg.nr_okregu) + '/' + powiat_nazwa + '/' + str(gmina.kod_gminy) + '/' +
         str(obwod.nr_obw) + '/', str(obwod))
        for obwod in Obwod.objects.filter(gmina=gmina)
    ]
    return render(request, 'wybory/wyniki.html', przygotuj_context(
        str(gmina), 'gminie', jednostki, linki, gmina.nazwa.upper(), wyniki_gminy(gmina), obwody))


def obwod_html(request, wojewodztwo_nazwa, nr_okregu, powiat_nazwa, kod_gminy, nr_obwodu):
    try:
        gmina = Gmina.objects.get(kod_gminy=kod_gminy)
    except Gmina.DoesNotExist:
        raise Http404("Obwód nie istnieje")
    try:
        okreg = Okreg.objects.get(nr_okregu=nr_okregu)
    except Okreg.DoesNotExist:
        raise Http404("Obwód nie istnieje")
    try:
        powiat = Powiat.objects.get(nazwa=powiat_nazwa)
    except Powiat.DoesNotExist:
        raise Http404("Obwód nie istnieje")
    try:
        obwod = Obwod.objects.get(gmina=gmina, nr_obw=nr_obwodu)
    except Obwod.DoesNotExist:
        raise Http404("Obwód nie istnieje")
    if obwod.gmina != gmina or gmina.powiat != powiat or powiat.okreg != okreg \
            or okreg.wojewodztwo.nazwa.lower() != wojewodztwo_nazwa:
        raise Http404("Obwód nie istnieje")

    jednostki = [
        ('/', 'POLSKA'),
        ('/' + wojewodztwo_nazwa + '/', wojewodztwo_nazwa.upper()),
        ('/' + wojewodztwo_nazwa + '/' + str(nr_okregu) + '/', okreg.nazwa.upper()),
        ('/' + wojewodztwo_nazwa + '/' + str(nr_okregu) + '/' + powiat_nazwa + '/', powiat_nazwa.upper()),
        ('/' + wojewodztwo_nazwa + '/' + str(nr_okregu) + '/' + powiat_nazwa + '/' + str(kod_gminy) + '/',
         gmina.nazwa.upper())
    ]
    context = {
        'nazwa_tytulowa_jednostki': str(obwod),
        'miejscownik': 'obwodzie',
        'jednostki': jednostki,
        'jednostka_menu': "OBWÓD NR " + str(nr_obwodu),
        'glosy_wazne': obwod.glosy_wazne,
        'wyniki': wyniki_obwod(obwod),
        'statystyka': statystyka(obwod.uprawnieni, obwod.wydane_karty, obwod.glosy_oddane, obwod.glosy_wazne)
    }
    return render(request, 'wybory/wyniki.html', context)
