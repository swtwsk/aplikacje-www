from django.db.models import Sum
from .models import *


def przygotuj_wyniki(przefiltrowane_wyniki):
    return [
        (str(Kandydat.objects.get(pk=wynik['kandydat'])), wynik['glosy']) for wynik in przefiltrowane_wyniki
    ]


def wyniki_kraju():
    return przygotuj_wyniki(
        WynikKandydata.objects.all().values('kandydat').annotate(
            glosy=Sum('glosy')).values('kandydat', 'glosy'))


def wyniki_wojewodztwa(wojewodztwo):
    return przygotuj_wyniki(
        WynikKandydata.objects.filter(obwod__gmina__powiat__okreg__wojewodztwo=wojewodztwo).values('kandydat').annotate(
            glosy=Sum('glosy')).values('kandydat', 'glosy'))


def wyniki_okregu(okreg):
    return przygotuj_wyniki(
        WynikKandydata.objects.filter(obwod__gmina__powiat__okreg=okreg).values('kandydat').annotate(
            glosy=Sum('glosy')).values('kandydat', 'glosy'))


def wyniki_powiatu(powiat):
    return przygotuj_wyniki(
        WynikKandydata.objects.filter(obwod__gmina__powiat=powiat).values('kandydat').annotate(
            glosy=Sum('glosy')).values('kandydat', 'glosy'))


def wyniki_gminy(gmina):
    return przygotuj_wyniki(WynikKandydata.objects.filter(obwod__gmina=gmina).values('kandydat').annotate(
        glosy=Sum('glosy')).values('kandydat', 'glosy'))


def wyniki_obwod(obwod):
    return przygotuj_wyniki(WynikKandydata.objects.filter(obwod=obwod).values('kandydat', 'glosy'))


def statystyka(uprawnieni, wydane_karty, glosy_oddane, glosy_wazne):
    return [
        ("Liczba uprawnionych do głosowania", uprawnieni),
        ("Liczba wydanych kart do głosowania", wydane_karty),
        ("Liczba kart wyjętych z urny", glosy_oddane),
        ("Liczba ważnych głosów", glosy_wazne),
        ("Liczba nieważnych głosów", glosy_oddane - glosy_wazne),
        ("Frekwencja (%)", "{0:.2f}".format(round(wydane_karty / uprawnieni * 100.0, 2)).replace('.', ','))
    ]


def statystyka_z_obwodow(obwody):
    uprawnieni = obwody.aggregate(Sum('uprawnieni'))['uprawnieni__sum']
    wydane_karty = obwody.aggregate(Sum('wydane_karty'))['wydane_karty__sum']
    glosy_oddane = obwody.aggregate(Sum('glosy_oddane'))['glosy_oddane__sum']
    glosy_wazne = obwody.aggregate(Sum('glosy_wazne'))['glosy_wazne__sum']
    return statystyka(uprawnieni, wydane_karty, glosy_oddane, glosy_wazne)


def przygotuj_context(nazwa_tytulowa, miejscownik, jednostki, linki, nazwa_menu, wyniki, obwody):
    return {
        'nazwa_tytulowa_jednostki': nazwa_tytulowa,
        'miejscownik': miejscownik,
        'jednostki': jednostki,
        'linki': linki,
        'jednostka_menu': nazwa_menu,
        'glosy_wazne': obwody.aggregate(Sum('glosy_wazne'))['glosy_wazne__sum'],
        'wyniki': wyniki,
        'statystyka': statystyka_z_obwodow(obwody)
    }
