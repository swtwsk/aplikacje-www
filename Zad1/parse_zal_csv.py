import os
import csv
import django

os.environ.setdefault("DJANGO_SETTINGS_MODULE", "Zad1.settings")
django.setup()

# noinspection PyPep8
from wybory.models import Kandydat, Wojewodztwo, Okreg

csv_path = "csv/kraj.csv"
KANDYDAT_LINE = 5


def parse_kandydat(kandydat_col):
    kandydat = kandydat_col.split()
    nazwisko = kandydat[0]
    imiona = ' '.join(kandydat[1:])
    Kandydat.objects.create(imie=imiona, nazwisko=nazwisko).save()


def parse_wojewodztwo(nazwa):
    nazwa[1:].lower()
    woj = Wojewodztwo.objects.create(nazwa=nazwa)
    woj.save()
    return woj


def parse_okreg(nr_okregu, nazwa, wojewodztwo):
    Okreg.objects.create(nazwa=nazwa, nr_okregu=nr_okregu, wojewodztwo=wojewodztwo).save()


with open(csv_path, 'r', encoding='utf-8') as csvfile:
    reader = csv.reader(csvfile, dialect='excel', delimiter=',')
    row_count = 1
    parse_okregi = False
    global w
    for row in reader:
        if row_count == KANDYDAT_LINE:
            for col in row:
                if col != "":
                    parse_kandydat(col)
        elif row[0] == "województwo":
            w = parse_wojewodztwo(row[1])
            parse_okregi = True
        elif parse_okregi:
            parse_okreg(int(row[0]), row[1], w)

        row_count += 1
