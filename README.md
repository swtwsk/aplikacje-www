# Aplikacje WWW
Projekty pisane na przedmiot **Aplikacje WWW** w semestrze letnim II roku studiów na **Uniwersytecie Warszawskim**

## Technologie

Python, Django


## Zadania
### Zadanie 1
Napisana w Django strona z wynikami wyborów prezydenckich z 2000 roku oraz skrypt w Pythonie generujący bazę danych na podstawie plików CSV. Projekt korzysta z SQLite'a i wbudowanego w Django ORMa.

### Zadanie 2
Napisana w Django strona obsługująca system rezerwacji lotów oraz skrypty wypełniające bazę danych na podstawie plików CSV, parsowania strony WWW oraz losowych danych. Aplikacja serwowana jest za pomocą Dockera, wykorzystując serwer Nginx, Gunicorna oraz PostgreSQL. Strona korzysta z Bootstrap CSS.
