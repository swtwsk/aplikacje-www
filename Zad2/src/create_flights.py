import os
import django
import datetime
import pytz
from random import randint
from django.db import transaction

os.environ.setdefault("DJANGO_SETTINGS_MODULE", "Zad2.settings")
django.setup()

from flights.models import Airport, Airplane, Flight

# given in task description:
MIN_TIMEDELTA = datetime.timedelta(minutes=30)
# courtesy of Wikipedia and http://info.flightmapper.net/route/YY_AKL_DOH :
MAX_TIMEDELTA = datetime.timedelta(hours=18, minutes=20)


def random_time():
    return datetime.time(hour=randint(0, 23), minute=randint(0, 59), second=randint(0, 59), tzinfo=pytz.UTC)


def random_date(since_date):
    start_date = since_date.toordinal()
    end_date = datetime.date(day=datetime.date.max.day, month=datetime.date.max.month, year=since_date.year).toordinal()
    date = datetime.date.fromordinal(randint(start_date, end_date))
    return datetime.datetime.combine(date, random_time())


def random_landing_datetime(start_datetime):
    return start_datetime + datetime.timedelta(seconds=randint(MIN_TIMEDELTA.seconds, MAX_TIMEDELTA.seconds))


with transaction.atomic():
    for airplane in Airplane.objects.all():
        for _ in range(50):
            takeoff_airport, landing_airport = Airport.objects.random_pair()
            while True:
                takeoff_datetime = random_date(datetime.date.today())
                landing_datetime = random_landing_datetime(takeoff_datetime)
                date_range = [takeoff_datetime, landing_datetime]
                if not Flight.objects.filter(airplane=airplane, takeoff_date__range=date_range) \
                        and not Flight.objects.filter(airplane=airplane, landing_date__range=date_range):
                    break

            Flight.objects.create(takeoff_airport=takeoff_airport, takeoff_date=takeoff_datetime,
                                  landing_airport=landing_airport, landing_date=landing_datetime,
                                  airplane=airplane)
