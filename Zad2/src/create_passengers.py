import os
import django
from random import choice, randint, sample
from django.db import transaction

os.environ.setdefault("DJANGO_SETTINGS_MODULE", "Zad2.settings")
django.setup()

from flights.models import Passenger, Flight, Booking

FIRST_NAMES = [
    "Andrzej",
    "Piotr",
    "Jakub",
    "Jan",
    "Filip",
    "Bartłomiej",
    "Tomasz",
    "Mateusz",
    "Szymon",
    "Jakub",
    "Juda",
    "Judasz",
    "Ruben",
    "Symeon",
    "Lewi",
    "Juda",
    "Issachar",
    "Zebulon",
    "Dan",
    "Naftali",
    "Gad",
    "Aszer",
    "Józef",
    "Beniamin",
]

SURNAMES = [
    "Rydel",
    "Mikołajczyk",
    "Tetmajer",
    "Przerwa-Tetmajer",
    "Starzewski",
    "Domański",
    "Singer",
    "Czepiec",
    "Susuł",
    "Nosowski",
    "Czajkowski",
    "Przybyszewski",
    "Wokulski",
    "Łęcki",
    "Rzecki",
    "Ochocki",
    "Krzeszowski",
    "Mincel",
    "Wysocki",
    "Węgiełek",
    "Geist",
    "Mraczewski",
    "Starski",
    "Szlangbaum",
    "Szuman",
    "Patkiewicz",
    "Maleski",
]

with transaction.atomic():
    for _ in range(Flight.objects.count()):
        Passenger.objects.get_or_create(first_name=choice(FIRST_NAMES), surname=choice(SURNAMES))

    passengers = Passenger.objects.all()
    for f in Flight.objects.all():
        flight_passengers = sample(list(passengers), 5)
        for p in flight_passengers:
            b = Booking(passenger=p, flight=f, tickets_count=randint(1, 4))
            b.save()
