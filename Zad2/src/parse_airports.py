import os
import csv
import django
from django.db import transaction

os.environ.setdefault("DJANGO_SETTINGS_MODULE", "Zad2.settings")
django.setup()

from flights.models import Airport

airports_data_path = "data/airports.dat"


with open(airports_data_path, 'r', encoding='utf-8') as airports_data_file:
    reader = csv.reader(airports_data_file, delimiter=',', quotechar='\"')
    to_end = False

    with transaction.atomic():
        for airport_info in reader:
            a = Airport.objects.create(
                icao=airport_info[5],
                country=airport_info[3],
                city=airport_info[2],
                name=airport_info[1],
                latitude=airport_info[6],
                longitude=airport_info[7]
            )
