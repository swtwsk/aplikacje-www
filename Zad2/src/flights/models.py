from django.db import models, transaction
from django.db.models import Count, Sum
from random import randint, sample
from .exceptions import TicketsException


class AirportManager(models.Manager):
    def random(self):
        count = self.aggregate(count=Count('icao'))['count']
        random_index = randint(0, count - 1)
        return self.all()[random_index]

    def random_pair(self):
        count = self.aggregate(count=Count('icao'))['count']
        indexes = sample(range(0, count - 1), 2)
        return self.all()[indexes[0]], self.all()[indexes[1]]


class Airport(models.Model):
    icao = models.CharField('ICAO', max_length=4, unique=True, primary_key=True)
    country = models.CharField(max_length=50, blank=False)
    city = models.CharField(max_length=50, blank=False)
    name = models.CharField(max_length=200, blank=False)
    latitude = models.FloatField(blank=True, null=True)
    longitude = models.FloatField(blank=True, null=True)

    objects = AirportManager()

    def __str__(self):
        return self.name + ", " + self.city + ", " + self.country


class AirplaneModel(models.Model):
    name = models.CharField(max_length=50, null=False, blank=False, unique=True)
    seats = models.IntegerField('number of seats', null=True, blank=True)

    def __str__(self):
        return self.name


class Airplane(models.Model):
    registration = models.CharField(max_length=7, unique=True, primary_key=True)
    model = models.ForeignKey(AirplaneModel, on_delete=models.CASCADE)

    def __str__(self):
        return self.registration


class Passenger(models.Model):
    first_name = models.CharField(max_length=25)
    surname = models.CharField(max_length=25)

    def __str__(self):
        return self.first_name + " " + self.surname

    class Meta:
        unique_together = ('first_name', 'surname')


class Flight(models.Model):
    takeoff_airport = models.ForeignKey(Airport, on_delete=models.CASCADE, related_name='flight_takeoff_airport')
    takeoff_date = models.DateTimeField()
    landing_airport = models.ForeignKey(Airport, on_delete=models.CASCADE, related_name='flight_landing_airport')
    landing_date = models.DateTimeField()
    airplane = models.ForeignKey(Airplane, on_delete=models.CASCADE)
    passenger_list = models.ManyToManyField(Passenger, through='Booking')

    class Meta:
        ordering = ['takeoff_date']


class Booking(models.Model):
    passenger = models.ForeignKey(Passenger, on_delete=models.CASCADE)
    flight = models.ForeignKey(Flight, on_delete=models.CASCADE)
    tickets_count = models.IntegerField()

    class Meta:
        unique_together = ('passenger', 'flight')

    @classmethod
    def add_tickets(cls, passenger, flight, tickets_count):
        with transaction.atomic():
            if cls.objects.filter(passenger=passenger, flight=flight).exists():
                booking = cls.objects.select_related('flight').select_for_update().get(passenger=passenger, flight=flight)
            else:
                booking = Booking.objects.create(
                    passenger=passenger,
                    flight=flight,
                    tickets_count=0,
                )
            if booking.flight.booking_set.aggregate(Sum('tickets_count'))['tickets_count__sum'] + tickets_count \
                    <= booking.flight.airplane.model.seats:
                booking.tickets_count = booking.tickets_count + tickets_count
            else:
                raise TicketsException

            booking.save()

        return booking
