from datetime import date
from django.shortcuts import render, get_object_or_404
from django.core.paginator import Paginator
from django.http import HttpResponseRedirect

from .models import *
from .forms import AddPassengerToFlightForm
from .exceptions import TicketsException


def flights_list(request):
    from_date = request.GET.get('from')
    to_date = request.GET.get('to')

    if from_date:
        if to_date:
            flights_all = Flight.objects.filter(takeoff_date__date__range=[from_date, to_date])
        else:
            flights_all = Flight.objects.filter(takeoff_date__date__range=[from_date, str(date.max)])
    elif to_date:
        flights_all = Flight.objects.filter(takeoff_date__date__range=[str(date.min), to_date])
    else:
        flights_all = Flight.objects.all()

    paginator = Paginator(flights_all, 25)

    page = request.GET.get('page')
    flights = paginator.get_page(page)
    context = {
        'flights': flights,
    }

    return render(request, 'flights/flights_list.html', context)


def flight_details(request, flight_pk):
    flight = get_object_or_404(Flight, pk=flight_pk)

    passengers = flight.passenger_list.all()
    passengers_and_tickets = []
    for passenger in passengers:
        passengers_and_tickets.append(passenger.booking_set.get(flight=flight))

    all_tickets_count = flight.booking_set.aggregate(Sum('tickets_count'))['tickets_count__sum']
    anonymous_add_attempt = False

    if request.method == 'POST':
        passenger_form = AddPassengerToFlightForm(request.POST)
        if request.user.is_authenticated:
            try:
                if passenger_form.is_valid():
                    # Create passenger ad hoc, if needed
                    passenger, _ = Passenger.objects.get_or_create(
                        first_name=passenger_form.cleaned_data['first_name'],
                        surname=passenger_form.cleaned_data['surname'],
                    )
                    tickets_count = passenger_form.cleaned_data['tickets_count']
                    Booking.add_tickets(passenger, flight, tickets_count)

                    return HttpResponseRedirect('')
            except TicketsException:
                passenger_form.add_error('tickets_count', 'Brak tylu wolnych miejsc')
        else:
            anonymous_add_attempt = True
    else:
        passenger_form = AddPassengerToFlightForm()

    context = {
        'flight_pk': flight_pk,
        'flight': flight,
        'passengers_and_tickets': passengers_and_tickets,
        'all_tickets_count': all_tickets_count,
        'airplane': flight.airplane,
        'passenger_form': passenger_form,
        'anonymous_add_attempt': anonymous_add_attempt,
    }

    return render(request, 'flights/flight_details.html', context)
