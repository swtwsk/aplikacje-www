from django.urls import path
from . import views

app_name = 'flights'

urlpatterns = [
    path('', views.flights_list, name='index'),
    path('details/<int:flight_pk>/', views.flight_details),
]
