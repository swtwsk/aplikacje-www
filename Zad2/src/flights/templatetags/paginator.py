from urllib.parse import urlencode
from django import template
register = template.Library()

PAGE_COUNT = 4


@register.filter(name='page_range')
def page_range(page_number, last_page_number):
    start = page_number - PAGE_COUNT
    if start <= 0:
        start = 1
    end = page_number + PAGE_COUNT
    if end > last_page_number:
        end = last_page_number
    return range(start, end + 1)


@register.simple_tag(takes_context=True)
def url_replace(context, **kwargs):
    query = context['request'].GET.dict()
    query.update(kwargs)
    return urlencode(query)
