from django import forms


class AddPassengerToFlightForm(forms.Form):
    first_name = forms.CharField(max_length=25, label='Imię')
    surname = forms.CharField(max_length=25, label='Nazwisko')
    tickets_count = forms.IntegerField(min_value=1, label='Ilość biletów')
