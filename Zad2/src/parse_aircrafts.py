from urllib import request
from bs4 import BeautifulSoup

import os
import django
from random import randint
from django.db import transaction

os.environ.setdefault("DJANGO_SETTINGS_MODULE", "Zad2.settings")
django.setup()

from flights.models import AirplaneModel, Airplane

url_page = "https://www.flightradar24.com/data/airlines/lo-lot/fleet"
url_request = request.Request(url_page, headers={"User-Agent": "Mozilla/5.0"})
page = request.urlopen(url_request)
soup = BeautifulSoup(page, 'html.parser')

with transaction.atomic():
    for s in soup.find_all('table', {"class": "table"}):
        tb = s.tbody
        for tr in tb.find_all('tr'):
            td = tr.find_all('td')
            registration = td[0].a.string
            aircraft_name = td[1].string
            try:
                model = AirplaneModel.objects.get(name=aircraft_name)
            except AirplaneModel.DoesNotExist:
                model = AirplaneModel.objects.create(name=aircraft_name, seats=randint(20, 180))
            Airplane.objects.create(registration=registration, model=model)
